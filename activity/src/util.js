function factorial(n){

    if(typeof n !== 'number') return undefined; //is to check if the value is number
    if(n < 0) return undefined; //
    if(n === 0) return 1;
    if(n === 1) return 1;

    return n * factorial(n-1);

}

function div_check(n){

    if(typeof n !== 'number') return false; //is to check if the value is number

    if(n%7 === 0 || n%5 === 0){ //check if number is divisible by 5 or 7
        return true;
    }else{
        return false;
    }

}

module.exports = {
    factorial: factorial,
    div_check: div_check
}

